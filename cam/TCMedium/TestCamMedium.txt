[PLAYER FORCE] blue
[HUMAN OPPONENT ALLOWED] no

[BLUE VICTORY TEXT] You won (blue)!
[BLUE DEFEAT TEXT] You Lost (blue)!

[RED VICTORY TEXT] You won (red)!
[RED DEFEAT TEXT] You Lost (red)!

/*Battle #1*/
[BATTLE NAME] Scenario1Medium
[WIN THRESHOLD] total defeat

[NEXT BATTLE IF WIN] Scenario2Medium
[NEXT BATTLE IF LOSE] // end campaign

[BLUE REFIT %] 99
[BLUE REPAIR VEHICLE %] 99
[BLUE RESUPPLY %] 99
[BLUE REST %] 99

[RED REFIT %] 17
[RED REPAIR VEHICLE %] 17
[RED RESUPPLY %] 17
[RED REST %] 17

/*Battle #2*/
[BATTLE NAME] Scenario2Medium
[WIN THRESHOLD] total defeat

[NEXT BATTLE IF WIN] // end campaign
[NEXT BATTLE IF LOSE] // end campaign

[BLUE REFIT %] 66
[BLUE REPAIR VEHICLE %] 66 
[BLUE RESUPPLY %] 66
[BLUE REST %] 66

[RED REFIT %] 11
[RED REPAIR VEHICLE %] 11
[RED RESUPPLY %] 11
[RED REST %] 11

