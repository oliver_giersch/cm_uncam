#![allow(dead_code)]

//TODO: write per campaign log file with all information (modules, mod tags, etc.)
//TODO: complete legacy and pre v120 module parsing
//TODO: complete data on modules and allow unknown modules (more robust, may apply some guard (no modules larger than 1000 e.g.)

macro_rules! debug {
    () => (if cfg!(debug_assertions) {print!("\n")});
    ($fmt:expr) => (if cfg!(debug_assertions) {print!(concat!($fmt, "\n"))});
    ($fmt:expr, $($arg:tt)*) => (if cfg!(debug_assertions) {print!(concat!($fmt, "\n"), $($arg)*)});
}

use std::env;
use std::fs;
use std::io::prelude::*;
use std::fs::File;

mod entity;
mod error;
mod extract;
mod log;
mod parse;
mod util;

use log::ErrorLogger;
use error::UnpackErr;
use std::path::Path;

/// Type alias for brevity
pub type Result<T> = std::result::Result<T, UnpackErr>;

fn main()
{
    let current_exe = env::current_exe()
        .expect("Critical filesystem error");
    let current_dir = current_exe.parent()
        .expect("Critical filesystem error");

    let mut logger = ErrorLogger::new();

    let entries = fs::read_dir(&current_dir).expect("Critical filesystem error");
    for entry in entries {
        let entry = entry.expect("Could not read file");
        let path = entry.path();

        if let Err(err) = parse_file(&path) {
            logger.push(err, path);
        }
    }

    if logger.has_errors() {
        debug!("There were parsing errors, creating logfile...");
        let mut logfile_path = current_dir.to_path_buf();
        logfile_path.push("uncam_error");
        logfile_path.set_extension("log");

        let mut logfile = File::create(logfile_path).expect("Could not create error log file. All is lost...");

        logfile.write_all(logger.to_string().as_bytes()).expect("Could not write to error log file. All is lost...");
        std::process::exit(1);
    }
}

/// Loop function. Check if file is campaign file, attempt to parse data, extract files.
#[inline]
fn parse_file(path: impl AsRef<Path>) -> Result<()>
{
    let path = path.as_ref();
    if path.is_dir() {
        return Ok(())
    }

    match (path.file_stem(), path.extension()) {
        (Some(filename), Some(ext)) if ext == "cam" => {
            let campaign = parse::parse_campaign(&path)?;
            extract::extract_campaign(&campaign, &path, &filename)?;
            Ok(())
        },
        _ => Ok(())
    }
}