use std::fmt;
use std::error::Error;
use std::path::PathBuf;

#[derive(Debug)]
pub struct ErrorLogger {
    errors : Vec<LoggedError>
}

impl ErrorLogger {
    #[inline]
    pub fn new() -> Self
    {
        ErrorLogger { errors : vec![] }
    }

    #[inline]
    pub fn push(&mut self, err: impl Error + 'static, path: PathBuf)
    {
        self.errors.push(LoggedError::new(err, path));
    }

    #[inline]
    pub fn has_errors(&self) -> bool
    {
        !self.errors.is_empty()
    }

    #[inline]
    pub fn errors(&self) -> &[LoggedError]
    {
        &self.errors
    }
}

impl fmt::Display for ErrorLogger {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        for error in &self.errors {
            write!(f, "In file: {}\n", error.filepath)?;
            write!(f, "{}\n", error.error.to_string())?;
        }

        write!(f, "")
    }
}

#[derive(Debug)]
pub struct LoggedError {
    pub filepath : String,
    pub error    : Box<dyn Error>
}

impl LoggedError {
    fn new(err: impl Error + 'static, path: PathBuf) -> Self
    {
        let path_string = path.into_os_string().into_string().expect("Could not parse file name to string.");

        LoggedError {
            filepath : path_string,
            error : Box::new(err)
        }
    }
}