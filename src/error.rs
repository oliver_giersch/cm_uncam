use std::io;
use std::error;
use std::fmt;

use util::PercentageError;

#[derive(Debug)]
pub enum UnpackErr {
    IO(io::Error),
    InvalidFileHeader(ErrorDescription),
    InvalidCode(ErrorDescription),
    InvalidPercentage(InvalidPercentageError),
    InvalidSize(ErrorDescription),
    InvalidZeroSize,
}

impl UnpackErr {
    #[inline]
    pub fn invalid_file_header(magic_number_1: u64, magic_number_2: u64) -> Self
    {
        let desc = String::from("Invalid header found at top of file.");
        let explanation = format!(
            concat!(
                "The first 16 bytes of every Combat Mission campaign or scenario file are always identical.\n",
                "\tExpected: 0x{:0x} and 0x{:0x}\n",
                "\t    Read: 0x{:0x} and 0x{:0x}"
            ),
            ::parse::HEADER_MAGIC_NUMBER_1, ::parse::HEADER_MAGIC_NUMBER_2, magic_number_1, magic_number_2
        );
        let hint = String::from("Either the read file is not a .cam or .btt file or it is a file with that extension that is not actually a combat mission file.");

        UnpackErr::InvalidFileHeader(ErrorDescription { desc, explanation, hint })
    }

    #[inline]
    pub fn invalid_code(origin: &str, code: u8, valid_codes: &[u8]) -> Self
    {
        let desc = format!("Invalid code: {}", origin);
        let explanation = format!(
            concat!(
                "The application attempted to parse some entity from a byte read in the current file.\n",
                "\tValid codes: {:?}\n",
                "\t  Read code: {}"
            ),
            valid_codes, code
        );
        let hint = String::from("Something probably went wrong with parsing the specific file format and the cursor was not positioned correctly.");

        UnpackErr::InvalidCode(ErrorDescription { desc, explanation, hint })
    }

    #[inline]
    pub fn invalid_size(origin: &str, size: usize, expected_size: usize) -> Self
    {
        let desc = format!("Invalid number read: {}", origin);
        let explanation = format!(
            concat!(
                "The application attempted to read a number value in the current campaign file was significantly larger than what could be be considered sensible.\n",
                "\tHeuristic for maximum value: {}\n",
                "\t                 Read value: {}"
            ),
            expected_size, size
        );
        let hint = String::from(
            concat!(
                "Something probably went wrong with parsing the specific file format and the cursor was not positioned correctly.\n",
                "Alternatively, the heuristic may not have been set conservatively enough and should be increased."
            )
        );

        UnpackErr::InvalidSize(ErrorDescription { desc, explanation, hint })
    }
}

impl fmt::Display for UnpackErr {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        match *self {
            UnpackErr::IO(ref err)                 => err.fmt(f),
            UnpackErr::InvalidFileHeader(ref desc) => write!(f, "{}", desc),
            UnpackErr::InvalidCode(ref desc)       => write!(f, "{}", desc),
            UnpackErr::InvalidPercentage(ref err)  => write!(f, "{}", err.desc),
            UnpackErr::InvalidZeroSize             => write!(f, "Attempted to read a required size value which was zero."),
            UnpackErr::InvalidSize(ref desc)       => write!(f, "{}", desc)
        }
    }
}

impl error::Error for UnpackErr {
    #[inline]
    fn cause(&self) -> Option<&dyn error::Error>
    {
        match *self {
            UnpackErr::InvalidPercentage(ref err) => Some(&err.cause),
            _ => None
        }
    }
}

impl From<io::Error> for UnpackErr {
    #[inline]
    fn from(err: io::Error) -> Self
    {
        UnpackErr::IO(err)
    }
}

impl From<PercentageError> for UnpackErr {
    #[inline]
    fn from(err: PercentageError) -> Self
    {
        let desc = String::from("Invalid percentage value");
        let explanation = format!(
            concat!(
                "The application attempted to parse a byte value which should represent an integer percentage value.\n",
                "\tValid range: 0-100\n",
                "\t Read value: {}"
            ),
            err.value()
        );
        let hint = String::from("Something probably went wrong with parsing the specific file format and the cursor was not positioned correctly.");

        UnpackErr::InvalidPercentage(InvalidPercentageError {
            desc  : ErrorDescription { desc, explanation, hint },
            cause : err }
        )
    }
}

#[derive(Debug)]
pub struct ErrorDescription {
    desc        : String,
    explanation : String,
    hint        : String
}

impl fmt::Display for ErrorDescription {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(f, "{}\n{}\n{}\n", self.desc, self.explanation, self.hint)
    }
}

#[derive(Debug)]
pub struct InvalidPercentageError {
    desc  : ErrorDescription,
    cause : PercentageError
}