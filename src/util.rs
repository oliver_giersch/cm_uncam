use std::error;
use std::fmt;
use std::mem;

const MAX_PERCENTAGE: u8 = 100;

/// Type for representing integer percentage values.
/// Can only be successfully created from 0 - 100.
#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
pub struct Percentage(u8);

/// Macro for implementing From<T> conversion for various integer/float types
macro_rules! impl_conversion {
    ($($t:ty), *) => {
        $(impl From<Percentage> for $t {
            #[inline]
            fn from(percentage: Percentage) -> Self
            {
                percentage.0 as Self
            }
        })*
    };
}

impl_conversion!(u8, i8, u16, i16, u32, i32, u64, i64, usize, isize, u128, i128);

impl Percentage {
    #[inline]
    pub fn try_from(value: u8) -> Result<Self, PercentageError>
    {
        if value > MAX_PERCENTAGE {
            return Err(PercentageError(value));
        }

        Ok(Percentage(value))
    }

    #[inline]
    pub fn value(&self) -> u8
    {
        self.0
    }
}

impl fmt::Display for Percentage {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(f, "{}",self.0)
    }
}

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
pub struct PercentageError(u8);

impl PercentageError {
    #[inline]
    pub fn value(&self) -> u8
    {
        self.0
    }
}

impl fmt::Display for PercentageError {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(f, "Tried to create percent value from illegal value {}", self.0)
    }
}

impl error::Error for PercentageError {}

/// Trait for parsing byte arrays into appropriately sized unsigned integers
pub trait ParseBytes where Self: Sized + Copy {
    type Into: Sized + Copy;

    /// Transmute the byte array into the Into type
    fn parse_bytes(&self) -> Self::Into;
}

macro_rules! impl_parse_bytes {
    ($($arr:ty => $uint:ty;)*) => {
        $(impl ParseBytes for $arr {
            type Into = $uint;

            #[inline]
            fn parse_bytes(&self) -> Self::Into
            {
                unsafe {
                    mem::transmute(*self)
                }
            }
        })*
    };
}

impl_parse_bytes!{
    [u8; 1]  => u8;
    [u8; 2]  => u16;
    [u8; 4]  => u32;
    [u8; 8]  => u64;
    [u8; 16] => u128;
}

#[cfg(test)]
mod test {
    use util::*;

    const SHORT  : [u8; 2] = [0x12, 0x00];
    const DOUBLE : [u8; 4] = [0x36, 0x54, 0x01, 0x00];
    const QUAD   : [u8; 8] = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];

    const SHORT_EXP  : u16 = 18;
    const DOUBLE_EXP : u32 = 87094;
    const QUAD_EXP   : u64 = 0;

    #[test]
    fn test_trait()
    {
        assert_eq!(SHORT_EXP, SHORT.parse_bytes());
        assert_eq!(DOUBLE_EXP, DOUBLE.parse_bytes());
        assert_eq!(QUAD_EXP, QUAD.parse_bytes());
    }
}