use ::Result;
use error::UnpackErr;

pub type ModuleID = u8;

const VALID_CODES: [u8; 8] = [
    GameCode::CMSF1 as u8,
    GameCode::CMA   as u8,
    GameCode::CMBN  as u8,
    GameCode::CMFI  as u8,
    GameCode::CMRT  as u8,
    GameCode::CMBS  as u8,
    GameCode::CMFB  as u8,
    GameCode::CMSF2 as u8
];

const INVALID_MODULE_CODE_ERR_MSG: &'static str = "Attempted to parse module code";

#[repr(u8)]
#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
pub enum GameCode {
    CMSF1 = 0,
    CMA   = 2,
    CMBN  = 4,
    CMFI  = 6,
    CMRT  = 8,
    CMBS  = 10,
    CMFB  = 12,
    CMSF2 = 14
}

/// Parse game code into appropriate enum variant.
#[inline]
pub fn parse_game_code(game_code: u8) -> Result<GameCode>
{
    use self::GameCode::*;
    match game_code {
         0 => Ok(CMSF1),
         2 => Ok(CMA),
         4 => Ok(CMBN),
         6 => Ok(CMFI),
         8 => Ok(CMRT),
        10 => Ok(CMBS),
        12 => Ok(CMFB),
        14 => Ok(CMSF2),
         c => Err(UnpackErr::invalid_code(
            "Attempted to parse game code.",
            c,
            &VALID_CODES
        ))
    }
}

/// Parse the game code and engine code
#[inline]
pub fn parse_game(game_code: GameCode, engine_code: u8) -> Box<dyn Game>
{
    match game_code {
        GameCode::CMSF1 => Box::new(CMSF1(engine_code)),
        GameCode::CMA   => Box::new(CMA(engine_code)),
        GameCode::CMBN  => Box::new(CMBN(engine_code)),
        GameCode::CMFI  => Box::new(CMFI(engine_code)),
        GameCode::CMRT  => Box::new(CMRT(engine_code)),
        GameCode::CMBS  => Box::new(CMBN(engine_code)),
        GameCode::CMFB  => Box::new(CMFB(engine_code)),
        GameCode::CMSF2 => Box::new(CMSF2(engine_code))
    }
}

/// Polymorphic/dynamic trait for representing different games and their specifics.
pub trait Game {
    /// Get the title of the game
    fn title(&self) -> &str;

    /// Get the abbreviated title of the game
    fn title_abbr(&self) -> &str;

    /// Get the patch version of the game
    fn patch_version(&self) -> &str;

    /// Get the code for the game
    fn game_code(&self) -> GameCode;

    /// Get the code for the engine version
    fn engine_version_code(&self) -> u8;

    /// Get the title of module for the given module code
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>;
}

pub struct CMSF1(u8);
pub struct CMA(u8);
pub struct CMBN(u8);
pub struct CMFI(u8);
pub struct CMRT(u8);
pub struct CMBS(u8);
pub struct CMFB(u8);
pub struct CMSF2(u8);

impl Game for CMSF1 {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Shock Force 1"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMSF 1"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
             0...1   => "v. 1.00 and earlier",
             2...35  => "v. 1.00 - v. 1.11",
            36...56  => "v. 1.11 - v. 1.21",
            57...79  => "v. 1.21 - v. 1.31",
            80...101 => "v. 1.31 - v. 1.32",
            _        => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMSF1
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            0x1 => Ok("Marines"),
            0x2 => Ok("British Forces"),
            0x4 => Ok("NATO"),
            _   => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[1, 2, 3, 4]
            ))
        }
    }
}

impl Game for CMA {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Afghanistan"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMA"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.0 {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMA
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        Err(UnpackErr::invalid_code(
            INVALID_MODULE_CODE_ERR_MSG,
            code,
            &[]
        ))
    }
}

impl Game for CMBN {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Battle for Normandy"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMBN"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMBN
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            1 => Ok("Commonwealth Forces"),
            2 => Ok("Market Garden"),
            3 => Ok("Vehicle Pack #1"),
            4 => Ok("Battle Pack #1"),
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[1, 2, 3, 4]
            ))
        }
    }
}

impl Game for CMFI {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Fortress Italy"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMFI"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMFI
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            1 => Ok("Gustav Line"),
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[1]
            ))
        }
    }
}

impl Game for CMRT {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Red Thunder"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMRT"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMRT
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[]
            ))
        }
    }
}

impl Game for CMBS {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Black Sea"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMBS"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMBS
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[]
            ))
        }
    }
}

impl Game for CMFB {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Final Blitzkrieg"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMFB"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMFB
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[]
            ))
        }
    }
}

impl Game for CMSF2 {
    #[inline]
    fn title(&self) -> &str
    {
        "Combat Mission: Shock Force 2"
    }

    #[inline]
    fn title_abbr(&self) -> &str
    {
        "CMSF2"
    }

    #[inline]
    fn patch_version(&self) -> &str
    {
        match self.engine_version_code() {
            _ => "unknown"
        }
    }

    #[inline]
    fn game_code(&self) -> GameCode
    {
        GameCode::CMSF2
    }

    #[inline]
    fn engine_version_code(&self) -> u8
    {
        self.0
    }

    #[inline]
    fn module_from_code(&self, code: ModuleID) -> Result<&'static str>
    {
        match code {
            _ if code <= 100 => Ok("unknown module"),
            _ => Err(UnpackErr::invalid_code(
                INVALID_MODULE_CODE_ERR_MSG,
                code,
                &[]
            ))
        }
    }
}