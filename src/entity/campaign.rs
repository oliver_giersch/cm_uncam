use std::fmt;

use error::UnpackErr;
use util::{Percentage, PercentageError};

#[derive(Debug, Clone)]
pub struct Campaign {
    pub header     : CampaignHeader,
    pub image_data : Option<Vec<u8>>,
    pub modules    : Option<Vec<String>>,
    pub mod_tags   : Option<Vec<String>>,
    pub scenarios  : Vec<Scenario>,
}

#[derive(Debug, Clone)]
pub struct CampaignHeader {
    pub title           : Option<String>,
    pub desc            : Option<String>,
    pub decision_blocks : Vec<DecisionBlock>
}

impl fmt::Display for CampaignHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>
    {
        write!(f,
            concat!(
                "[PLAYER FORCE] {}\n",
                "[HUMAN OPPONENT ALLOWED] no\n\n",
                "[BLUE VICTORY TEXT] {}\n",
                "[BLUE DEFEAT TEXT] {}\n\n",
                "[RED VICTORY TEXT] {}\n",
                "[RED DEFEAT TEXT] {}\n\n"
            ), "unknown", "unknown", "unknown", "unknown", "unknown"
        )?;

        for decision_block in &self.decision_blocks {
            write!(f,
                concat!(
                    "/* Battle #{number} */\n",
                    "[BATTLE NAME] {name}\n",
                    "[WIN THRESHOLD] {threshold}\n\n",
                    "[NEXT BATTLE IF WIN] {win}\n",
                    "[NEXT BATTLE IF LOSE] {lose}\n\n",
                    "{blue_refit}\n",
                    "{red_refit}\n"
                ),
                number = decision_block.scenario_idx + 1,
                name = decision_block.scenario_filename,
                threshold = decision_block.win_threshold,
                win = match decision_block.next_battle_win_idx {
                    Some(idx) => &self.decision_blocks[idx as usize].scenario_filename,
                    None      => "// campaign end"
                },
                lose = match decision_block.next_battle_lose_idx {
                    Some(idx) => &self.decision_blocks[idx as usize].scenario_filename,
                    None      => "// campaign end"
                },
                blue_refit = decision_block.blue_refit,
                red_refit = decision_block.red_refit
            )?;
        }

        write!(f, "")
    }
}

#[derive(Debug, Clone)]
pub struct DecisionBlock {
    pub scenario_idx         : usize,
    pub scenario_filename    : String,
    pub win_threshold        : BattleResult,
    pub next_battle_win_idx  : Option<u16>,
    pub next_battle_lose_idx : Option<u16>,
    pub blue_refit           : RefitSettings,
    pub red_refit            : RefitSettings
}

#[derive(Copy, Clone, Debug, PartialOrd, PartialEq)]
pub enum Side {
    Blue,
    Red
}

impl Side {
    #[inline]
    pub fn as_str(&self) -> &str
    {
        match *self {
            Side::Blue => "blue",
            Side::Red  => "red"
        }
    }
}

impl fmt::Display for Side {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>
    {
        match *self {
            Side::Blue => write!(f, "BLUE"),
            Side::Red  => write!(f, "RED")
        }
    }
}

const VALID_CODES: [u8; 10] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

#[derive(Copy, Clone, Debug, PartialOrd, PartialEq)]
#[repr(u8)]
pub enum BattleResult {
    TotalDefeat     = 0,
    MajorDefeat     = 1,
    TacticalDefeat  = 2,
    MinorDefeat     = 3,
    Draw            = 4,
    MinorVictory    = 5,
    TacticalVictory = 6,
    MajorVictory    = 7,
    TotalVictory    = 8,
    ScoreDetermined = 9
}

impl BattleResult {
    #[inline]
    pub fn try_from_byte(byte: u8) -> Result<Self, UnpackErr>
    {
        use entity::campaign::BattleResult::*;
        match byte {
            0 => Ok(TotalDefeat),
            1 => Ok(MajorDefeat),
            2 => Ok(TacticalDefeat),
            3 => Ok(MinorDefeat),
            4 => Ok(Draw),
            5 => Ok(MinorVictory),
            6 => Ok(TacticalVictory),
            7 => Ok(MajorVictory),
            8 => Ok(TotalVictory),
            9 => Ok(ScoreDetermined),
            c => Err(UnpackErr::invalid_code(
                "Attempted to parse battle result",
                c,
                &VALID_CODES
            ))
        }
    }
}

impl fmt::Display for BattleResult {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>
    {
        use entity::campaign::BattleResult::*;
        match *self {
            TotalDefeat     => write!(f, "total defeat"),
            MajorDefeat     => write!(f, "major defeat"),
            TacticalDefeat  => write!(f, "tactical defeat"),
            MinorDefeat     => write!(f, "minor defeat"),
            Draw            => write!(f, "draw"),
            MinorVictory    => write!(f, "minor victory"),
            TacticalVictory => write!(f, "tactical victory"),
            MajorVictory    => write!(f, "major victory"),
            TotalVictory    => write!(f, "total victory"),
            ScoreDetermined => write!(f, "determined from overall score")
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct RefitSettings {
    pub side     : Side,
    pub refit    : Percentage,
    pub repair   : Percentage,
    pub resupply : Percentage,
    pub rest     : Percentage
}

impl RefitSettings {
    #[inline]
    pub fn try_from_bytes(side: Side, bytes: [u8; 4]) -> Result<Self, PercentageError>
    {
        let refit = Percentage::try_from(bytes[0])?;
        let repair = Percentage::try_from(bytes[1])?;
        let resupply = Percentage::try_from(bytes[2])?;
        let rest = Percentage::try_from(bytes[3])?;

        Ok(RefitSettings { side, refit, repair, resupply, rest })
    }
}

impl fmt::Display for RefitSettings {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error>
    {
        write!(f,
            concat!(
                "[{side} REFIT %] {refit}\n",
                "[{side} REPAIR VEHICLE %] {repair}\n",
                "[{side} RESUPPLY %] {resupply}\n",
                "[{side} REST %] {rest}\n",
            ), side = self.side, refit = self.refit, repair = self.repair, resupply = self.resupply, rest = self.rest
        )
    }
}

#[derive(Debug, Clone)]
pub struct Scenario {
    pub idx  : usize,
    pub data : Vec<u8>
}