mod campaign;
mod game;

pub use entity::campaign::{
    BattleResult,
    Campaign,
    CampaignHeader,
    DecisionBlock,
    RefitSettings,
    Scenario,
    Side
};

pub use entity::game::{Game, GameCode, parse_game, parse_game_code};