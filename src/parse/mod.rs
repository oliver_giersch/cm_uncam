use std::io::{self, prelude::*};
use std::fs::File;
use std::path::Path;

mod campaign;
mod string;

use ::Result;

use entity::{Campaign, Game, parse_game, parse_game_code};
use error::UnpackErr;
use util::ParseBytes;

pub enum AbsolutePositions {
    HeaderMagicNumber1 = 0x0,
    HeaderMagicNumber2 = 0x8,
    GameCode           = 0x16,
    EngineCode         = 0x18
}

const FILE_SIZE_BUFFER: usize = 4;
const MAGIC_NUM_SIZE: usize = 8;

/// Reverse ordering from file header (little endian!)
pub const HEADER_MAGIC_NUMBER_1: u64 = 0xC8C4F9A5D0132E37;
pub const HEADER_MAGIC_NUMBER_2: u64 = 0x4AF6E5B261708D9B;

/// Parse all relevant campaign data and files into a fitting data structure
pub fn parse_campaign(filepath: impl AsRef<Path>) -> Result<Campaign>
{
    let mut file = CampaignFile::open(&filepath)?;

    let game = parse_campaign_header(&mut file)?;

    debug!("Filename: {}", &filepath.as_ref().display());
    debug!("Game: {} ({})", game.title(), game.title_abbr());
    debug!("Version: {} (code = {})", game.patch_version(), game.engine_version_code());

    let campaign = campaign::parse(&*game, &mut file)?;
    Ok(campaign)
}

/// Parses the header of the campaign file and the game and version
/// bytes (first 20 bytes)
fn parse_campaign_header(file: &mut CampaignFile) -> Result<Box<dyn Game>>
{
    let mut header_buffer= [0; MAGIC_NUM_SIZE];

    file.handle.read_exact(&mut header_buffer)?;
    let magic_number_1 = header_buffer.parse_bytes();

    file.handle.read_exact(&mut header_buffer)?;
    let magic_number_2 = header_buffer.parse_bytes();

    if HEADER_MAGIC_NUMBER_1 != magic_number_1 || HEADER_MAGIC_NUMBER_2 != magic_number_2 {
        return Err(UnpackErr::invalid_file_header(magic_number_1, magic_number_2));
    }

    let mut version_buffer = [0; 2];

    file.handle.read_exact(&mut version_buffer)?;
    let game_version = version_buffer[0];

    file.handle.read_exact(&mut version_buffer)?;
    let engine_version = version_buffer[0];

    let game_code = parse_game_code(game_version)?;
    let game = parse_game(game_code, engine_version);
    Ok(game)
}

#[derive(Debug)]
pub struct CampaignFile {
    pub handle : File,
    pub size   : u64
}

impl CampaignFile {
    fn open(filepath: impl AsRef<Path>) -> io::Result<CampaignFile>
    {
        let handle = File::open(&filepath)?;
        let size = handle.metadata()?
            .len();

        Ok(CampaignFile { handle, size })
    }
}