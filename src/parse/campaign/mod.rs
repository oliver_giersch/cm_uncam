use std::io::prelude::*;
use std::marker::PhantomData;

mod layout;
mod scenario;

use ::Result;

use entity::{Campaign, CampaignHeader, Game, GameCode};
use error::UnpackErr;
use parse::{
    FILE_SIZE_BUFFER,
    campaign::{
        layout::{
            DefaultDecisionBlockLayout,
            DefaultIntermediateLayout,
            DefaultModuleLayout,
            DecisionBlockLayout,
            IntermediateLayout,
            LegacyDecisionBlockLayout,
            LegacyIntermediateLayout,
            LegacyModuleLayout,
            ModuleLayout,
            PreV120ModuleLayout,
            PreV130IntermediateLayout
        },
        scenario::{ScenarioParse}
    },
    CampaignFile,
    string
};
use util::ParseBytes;

/// Legacy game parsing (CMSF1 & CMA)
type LegacyParsePreV48  = CampaignParse<LegacyModuleLayout, LegacyIntermediateLayout, LegacyDecisionBlockLayout>;
type LegacyParseV48     = CampaignParse<LegacyModuleLayout, PreV130IntermediateLayout, LegacyDecisionBlockLayout>;
type LegacyParsePostV48 = CampaignParse<LegacyModuleLayout, PreV130IntermediateLayout, DefaultDecisionBlockLayout>;

/// Parsing for all games since CMBN
type ParsePreV120  = CampaignParse<PreV120ModuleLayout, PreV130IntermediateLayout, DefaultDecisionBlockLayout>;
type ParsePreV130  = CampaignParse<DefaultModuleLayout, PreV130IntermediateLayout, DefaultDecisionBlockLayout>;
type ParsePostV130 = CampaignParse<DefaultModuleLayout, DefaultIntermediateLayout, DefaultDecisionBlockLayout>;

/// Parse the campaign in the given file for the appropriate game version
///
/// Uses legacy parsing for CMSF1 and regular (engine version dependent)
/// parsing for all later games.
#[inline]
pub fn parse(game: &dyn Game, file: &mut CampaignFile) -> Result<Campaign>
{
    const LIM: u8 = u8::max_value();

    if game.game_code() == GameCode::CMSF1 || game.game_code() == GameCode::CMA {
        return match game.engine_version_code() {
             0...47  => LegacyParsePreV48::parse_campaign(game, file),
            48       => LegacyParseV48::parse_campaign(game, file),
            49...LIM => LegacyParsePostV48::parse_campaign(game, file),
            _        => unreachable!()
        };
    }

    match game.engine_version_code() {
         99...119 => ParsePreV120::parse_campaign(game, file),
        120...129 => ParsePreV130::parse_campaign(game, file),
        130...LIM => ParsePostV130::parse_campaign(game, file),
        _         => unreachable!()
    }
}

#[derive(Debug)]
struct CampaignParse<M, I, D>(PhantomData<(M, I, D)>);

impl<M, I, D> CampaignParse<M, I, D> where
    M: ModuleLayout,
    I: IntermediateLayout,
    D: DecisionBlockLayout
{
    /// Parse the campaign according to the specified generic layout types.
    pub fn parse_campaign(game: &dyn Game, file: &mut CampaignFile) -> Result<Campaign>
    {
        let modules = M::parse_modules(game, file)?;
        let title = string::try_parse_string(file)?;
        let desc = string::try_parse_string(file)?;
        let image_data = Self::parse_image(file)?;
        let mod_tags = I::parse_mod_tags(file)?;

        I::parse_intermediate_bytes(file)?;
        Self::parse_core_units(file)?;

        let (decision_blocks, scenarios) = ScenarioParse::<D>::parse_scenario_blocks(file)?;

        Ok(Campaign {
            header : CampaignHeader { title, desc, decision_blocks },
            image_data,
            modules,
            mod_tags,
            scenarios,
        })
    }

    /// Parse the optional campaign image and return the image data if it exists.
    ///
    /// Before the image data are 4 bytes specifying the size of the image.
    /// If the image size is 0, the returned option is None.
    fn parse_image(file: &mut CampaignFile) -> Result<Option<Vec<u8>>>
    {
        const MAX_IMAGE_SIZE_HEURISTIC: usize = 100_000;

        debug!("parsing campaign image...");
        let mut image_size_buffer = [0; FILE_SIZE_BUFFER];
        file.handle.read_exact(&mut image_size_buffer)?;

        let image_size = image_size_buffer.parse_bytes() as usize;
        debug!("image size in bytes {}", image_size);

        if image_size == 0 {
            return Ok(None);
        }

        if image_size >= MAX_IMAGE_SIZE_HEURISTIC {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse campaign image size",
                image_size,
                MAX_IMAGE_SIZE_HEURISTIC
            ));
        }

        let mut image_buffer = vec![0; image_size];
        file.handle.read_exact(&mut image_buffer)?;

        Ok(Some(image_buffer))
    }

    /// Parse the core units block.
    ///
    /// A variable amount of bytes is taken up by the campaign core units specification.
    fn parse_core_units(file: &mut CampaignFile) -> Result<()>
    {
        const MAX_CORE_UNITS_SIZE_HEURISTIC: usize = 100_000_000;

        debug!("parsing core units...");
        let mut core_units_size_buffer = [0; FILE_SIZE_BUFFER];
        file.handle.read_exact(&mut core_units_size_buffer)?;

        let core_units_size = core_units_size_buffer.parse_bytes() as usize;
        debug!("core units block size: {}", core_units_size);
        if core_units_size == 0 {
            return Ok(());
        }

        if core_units_size >= MAX_CORE_UNITS_SIZE_HEURISTIC {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse core units size",
                core_units_size,
                MAX_CORE_UNITS_SIZE_HEURISTIC
            ));
        }

        let mut core_units_buffer = vec![0; core_units_size];
        file.handle.read_exact(&mut core_units_buffer)?;

        Ok(())
    }
}