use std::io::prelude::*;

use ::Result;

use error::UnpackErr;
use parse::{CampaignFile, FILE_SIZE_BUFFER, string};
use util::ParseBytes;

pub trait IntermediateLayout {
    const INTERMEDIATE_BUFFER_SIZE: usize = 19;

    fn parse_mod_tags(file: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        const IMG_MOD_TAG_OFFSET: usize = 3;
        const MAX_MOD_TAGS_HEURISTIC: usize = 10_000;

        debug!("parsing mod tags...");
        let mut offset_buffer = [0; IMG_MOD_TAG_OFFSET];
        file.handle.read_exact(&mut offset_buffer)?;

        let mut mod_tag_count_buffer = [0; FILE_SIZE_BUFFER];
        file.handle.read_exact(&mut mod_tag_count_buffer)?;

        let mod_tag_count = mod_tag_count_buffer.parse_bytes() as usize;
        debug!("mod tag count: {}", mod_tag_count);

        if mod_tag_count == 0 {
            return Ok(None);
        }

        if mod_tag_count >= MAX_MOD_TAGS_HEURISTIC {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse mod tags number",
                mod_tag_count,
                MAX_MOD_TAGS_HEURISTIC
            ));
        }

        let mut mod_tags = Vec::with_capacity(mod_tag_count);
        for _ in 0..mod_tag_count {
            let mod_tag = string::parse_string(file)?;
            mod_tags.push(mod_tag);
        }

        Ok(Some(mod_tags))
    }

    fn parse_intermediate_bytes(file: &mut CampaignFile) -> Result<()>
    {
        let mut intermediate_buffer = vec![0; Self::INTERMEDIATE_BUFFER_SIZE];
        file.handle.read_exact(&mut intermediate_buffer)?;

        Ok(())
    }
}

#[derive(Debug)]
pub struct DefaultIntermediateLayout;
impl IntermediateLayout for DefaultIntermediateLayout {}

#[derive(Debug)]
pub struct LegacyIntermediateLayout;
impl IntermediateLayout for LegacyIntermediateLayout {
    const INTERMEDIATE_BUFFER_SIZE: usize = 19;

    fn parse_mod_tags(_: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        Ok(None)
    }
}

#[derive(Debug)]
pub struct PreV130IntermediateLayout;
impl IntermediateLayout for PreV130IntermediateLayout {
    const INTERMEDIATE_BUFFER_SIZE: usize = 21;

    fn parse_mod_tags(_: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        Ok(None)
    }
}