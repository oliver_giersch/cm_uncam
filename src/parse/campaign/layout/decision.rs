use std::io::prelude::*;

use ::Result;

use parse::{CampaignFile};

pub trait DecisionBlockLayout {
    #[inline]
    fn parse_intermediate_block(file: &mut CampaignFile) -> Result<()>
    {
        const INTERMEDIATE_BUFFER_SIZE: usize = 2;
        let mut intermediate_buffer = [0; INTERMEDIATE_BUFFER_SIZE];

        file.handle.read_exact(&mut intermediate_buffer)?;
        Ok(())
    }
}

#[derive(Debug)]
pub struct DefaultDecisionBlockLayout;
impl DecisionBlockLayout for DefaultDecisionBlockLayout {}

#[derive(Debug)]
pub struct LegacyDecisionBlockLayout;
impl DecisionBlockLayout for LegacyDecisionBlockLayout {
    #[inline]
    fn parse_intermediate_block(_: &mut CampaignFile) -> Result<()>
    {
        Ok(())
    }
}