use std::io::prelude::*;

use ::Result;

use entity::Game;
use error::UnpackErr;
use parse::CampaignFile;
use util::ParseBytes;

/// Trait for implementing version specific parsing of the campaign file section
/// specifying the modules required by the campaign.
///
/// Default implementation is for post engine version 120.
pub trait ModuleLayout {
    #[inline]
    fn parse_modules(game: &dyn Game, file: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        const INTERMEDIATE_BUFFER_HEAD_SIZE: usize = 5;
        const INTERMEDIATE_BUFFER_TAIL_SIZE: usize = 20;
        const MODULE_COUNT_BUFFER_SIZE: usize = 2;
        const MAX_MODULE_COUNT: usize = 4;

        debug!("parsing modules...");
        let mut intermediate_buffer_head = [0; INTERMEDIATE_BUFFER_HEAD_SIZE];
        file.handle.read_exact(&mut intermediate_buffer_head)?;

        let mut module_count_buffer = [0; MODULE_COUNT_BUFFER_SIZE];
        file.handle.read_exact(&mut module_count_buffer)?;

        let module_count = module_count_buffer.parse_bytes() as usize;
        if module_count > MAX_MODULE_COUNT {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse module count",
                module_count,
                MAX_MODULE_COUNT
            ));
        }

        let mut module_buffer = vec![0; module_count];
        file.handle.read_exact(&mut module_buffer)?;

        let modules = module_buffer.into_iter()
            .map(|code| game.module_from_code(code).map(|string| String::from(string)))
            .collect::<Result<Vec<String>>>()?;

        let mut intermediate_buffer_tail = [0; INTERMEDIATE_BUFFER_TAIL_SIZE];
        file.handle.read_exact(&mut intermediate_buffer_tail)?;

        Ok(Some(modules))
    }
}

#[derive(Debug)]
pub struct DefaultModuleLayout;
impl ModuleLayout for DefaultModuleLayout {}

#[derive(Debug)]
pub struct LegacyModuleLayout;
impl ModuleLayout for LegacyModuleLayout {
    #[inline]
    fn parse_modules(game: &dyn Game, file: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        const MODULE_BUFFER_SIZE : usize = 21;
        const MODULE_CODES       : [u8; 3] = [1, 2, 4];
        const MODULE_IDX         : usize = 1;

        let mut module_buffer = [0; MODULE_BUFFER_SIZE];
        file.handle.read_exact(&mut module_buffer)?;

        let code = module_buffer[MODULE_IDX];
        if code == 0 {
            return Ok(None);
        }

        let modules = MODULE_CODES.iter()
            .filter(|&&template| code & template != 0)
            .map(|&code| game.module_from_code(code).map(|string| String::from(string)))
            .collect::<Result<Vec<String>>>()?;

        Ok(Some(modules))
    }
}

#[derive(Debug)]
pub struct PreV120ModuleLayout;
impl ModuleLayout for PreV120ModuleLayout {
    #[inline]
    fn parse_modules(game: &dyn Game, file: &mut CampaignFile) -> Result<Option<Vec<String>>>
    {
        const MODULE_BUFFER_SIZE: usize = 23;
        const MODULE_COUNT_IDX   : usize = 5;
        const MODULE_INDICES     : [usize; 3] = [1, 6, 7];

        let mut module_buffer = [0; MODULE_BUFFER_SIZE];
        file.handle.read_exact(&mut module_buffer)?;

        let module_count = module_buffer[MODULE_COUNT_IDX] as usize;
        if module_count == 0 {
            return Ok(None);
        }

        if module_count > MODULE_INDICES.len() {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse module count",
                module_count,
                MODULE_INDICES.len()
            ));
        }

        let modules = MODULE_INDICES.iter()
            .take(module_count)
            .map(|&index| module_buffer[index])
            .map(|code| game.module_from_code(code).map(|string| String::from(string)))
            .collect::<Result<Vec<String>>>()?;

        Ok(Some(modules))
    }
}