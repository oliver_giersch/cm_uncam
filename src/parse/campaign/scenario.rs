use std;
use std::io::prelude::*;
use std::marker::PhantomData;

use ::Result;

use entity::{BattleResult, DecisionBlock, RefitSettings, Side, Scenario};
use error::UnpackErr;
use parse::{
    CampaignFile,
    campaign::layout::DecisionBlockLayout,
    FILE_SIZE_BUFFER,
    string
};
use util::ParseBytes;

/// Container for a campaign decision block and a scenario
#[derive(Debug)]
pub struct ScenarioBlock(pub DecisionBlock, pub Scenario);

/// Scenario block parsing functionality
#[derive(Debug)]
pub struct ScenarioParse<D> where D: DecisionBlockLayout {
    _marker : PhantomData<D>
}

impl<D> ScenarioParse<D> where D: DecisionBlockLayout {
    pub fn parse_scenario_blocks(file: &mut CampaignFile) -> Result<(Vec<DecisionBlock>, Vec<Scenario>)>
    {
        const MAX_SCENARIO_HEURISTIC: usize = 1_000;

        let mut scenario_count_buffer = [0; FILE_SIZE_BUFFER];
        file.handle.read_exact(&mut scenario_count_buffer)?;

        let scenario_count = scenario_count_buffer.parse_bytes() as usize;
        if scenario_count == 0 {
            return Err(UnpackErr::InvalidZeroSize);
        }

        if scenario_count >= MAX_SCENARIO_HEURISTIC {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse scenario number",
                scenario_count,
                MAX_SCENARIO_HEURISTIC
            ));
        }

        let mut decision_blocks = Vec::with_capacity(scenario_count);
        let mut scenarios = Vec::with_capacity(scenario_count);

        let iter = ScenarioIter::<D>::new(file, scenario_count);
        for next in iter {
            let ScenarioBlock(decision_block, scenario) = next?;
            decision_blocks.push(decision_block);
            scenarios.push(scenario);
        }

        Ok((decision_blocks, scenarios))
    }
}

#[derive(Debug)]
struct ScenarioIter<'f, D> where D: DecisionBlockLayout {
    file  : &'f mut CampaignFile,
    count : usize,
    current   : usize,
    _marker   : PhantomData<D>
}

impl<'f, D> Iterator for ScenarioIter<'f, D> where D: DecisionBlockLayout {
    type Item = Result<ScenarioBlock>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item>
    {
        if self.current == self.count {
            return None;
        }

        debug!("parsing next scenario block...");
        let result = self.try_next_block();
        self.current += 1;

        Some(result)
    }
}

impl<'f, D> ScenarioIter<'f, D> where D: DecisionBlockLayout {
    #[inline]
    pub fn new(file: &'f mut CampaignFile, count: usize) -> Self
    {
        ScenarioIter { file, count, current: 0, _marker: PhantomData }
    }

    #[inline]
    fn try_next_block(&mut self) -> Result<ScenarioBlock>
    {
        let decision_block = self.try_next_decision_block()?;
        let data = self.try_next_scenario()?;

        Ok(ScenarioBlock(decision_block, Scenario { idx: self.current, data }))
    }

    #[inline]
    fn try_next_decision_block(&mut self) -> Result<DecisionBlock>
    {
        const BATTLE_RESULT_SIZE: usize = 1;
        const BATTLE_IDX_SIZE: usize = 2;

        let filename = string::parse_string(self.file)?;

        let mut win_threshold_buffer = [0; BATTLE_RESULT_SIZE];

        self.file.handle.read_exact(&mut win_threshold_buffer)?;
        let win_threshold = BattleResult::try_from_byte(win_threshold_buffer[0])?;

        let mut next_battle_buffer = [0; BATTLE_IDX_SIZE];

        self.file.handle.read_exact(&mut next_battle_buffer)?;
        let next_battle_win_idx = match next_battle_buffer.parse_bytes() {
            std::u16::MAX => None,
            idx           => Some(idx)
        };

        self.file.handle.read_exact(&mut next_battle_buffer)?;
        let next_battle_lose_idx = match next_battle_buffer.parse_bytes() {
            std::u16::MAX => None,
            idx           => Some(idx)
        };

        D::parse_intermediate_block(self.file)?;

        let mut refit_settings_buffer = [0; FILE_SIZE_BUFFER];

        self.file.handle.read_exact(&mut refit_settings_buffer)?;
        let blue_refit = RefitSettings::try_from_bytes(Side::Blue, refit_settings_buffer)?;

        self.file.handle.read_exact(&mut refit_settings_buffer)?;
        let red_refit = RefitSettings::try_from_bytes(Side::Red, refit_settings_buffer)?;

        Ok(DecisionBlock {
            scenario_idx : self.current,
            scenario_filename : filename,
            win_threshold,
            next_battle_win_idx,
            next_battle_lose_idx,
            blue_refit,
            red_refit,
        })
    }

    #[inline]
    fn try_next_scenario(&mut self) -> Result<Vec<u8>>
    {
        let mut scenario_size_buffer = [0; FILE_SIZE_BUFFER];

        self.file.handle.read_exact(&mut scenario_size_buffer)?;
        let scenario_size = scenario_size_buffer.parse_bytes() as usize;

        debug!("scenario size in bytes: {:#?}\n", scenario_size);
        if scenario_size >= self.file.size as usize {
            return Err(UnpackErr::invalid_size(
                "Attempted to parse scenario file size (campaign file size as maximum size heuristic)",
                scenario_size,
                self.file.size as usize
            ));
        }

        let mut scenario_data_buffer = vec![0; scenario_size];

        self.file.handle.read_exact(&mut scenario_data_buffer)?;
        Ok(scenario_data_buffer)
    }
}