use std::io::prelude::*;

use ::Result;
use error::UnpackErr;
use parse::CampaignFile;
use util::ParseBytes;

const STR_SIZE_BUFFER: usize = 2;

/// Parse a required (non-optional) string from the given file at the current cursor.
///
/// The layout requires the first 2 bytes to indicate the size of the string.
pub fn parse_string(file: &mut CampaignFile) -> Result<String>
{
    debug!("parsing required string...");
    let mut size_buffer = [0; STR_SIZE_BUFFER];
    file.handle.read_exact(&mut size_buffer)?;

    let str_size = size_buffer.parse_bytes() as usize;
    debug!("string size: {}", str_size);
    debug_assert!(str_size <= 1_000);

    if str_size == 0 {
        return Err(UnpackErr::InvalidZeroSize);
    }

    let mut str_buffer = vec![0; str_size];
    file.handle.read_exact(&mut str_buffer)?;

    let string = str_buffer.into_iter()
        .map(|byte| byte as char)
        .collect();

    debug!("{}", string);
    Ok(string)
}

/// Parse an optional string from the given file at the current cursor.
///
/// The layout requires the first 2 bytes to indicate the size of the string.
pub fn try_parse_string(file: &mut CampaignFile) -> Result<Option<String>>
{
    debug!("parsing optional string...");
    let mut size_buffer = [0; STR_SIZE_BUFFER];
    file.handle.read_exact(&mut size_buffer)?;

    let str_size = size_buffer.parse_bytes() as usize;
    debug!("string size: {}", str_size);
    debug_assert!(str_size <= 1_000);

    if str_size == 0 {
        return Ok(None);
    }

    let mut str_buffer = vec![0; str_size];
    file.handle.read_exact(&mut str_buffer)?;

    let string = str_buffer.into_iter()
        .map(|byte| byte as char)
        .collect();

    debug!("{}", string);
    Ok(Some(string))
}