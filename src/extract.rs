use std::fs;

use std::fs::File;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::io::prelude::*;

use ::Result;

use entity::Campaign;

const IMAGE_FILE_EXT    : &'static str = "bmp";
const SCENARIO_FILE_EXT : &'static str = "btt";

/// Write a directory, the campaign header file, the image file, a metadata file and all scenario
/// files.
pub fn extract_campaign(
    campaign: &Campaign,
    filepath: impl AsRef<Path>,
    filename: impl AsRef<OsStr>
) -> Result<()>
{
    debug!("filepath: {}", filepath.as_ref().display());
    debug!("filename: {}", filename.as_ref().to_str().unwrap());

    let campaign_dir = filepath.as_ref().with_file_name(filename.as_ref());
    debug!("dir name: {}", campaign_dir.display());

    fs::create_dir_all(&campaign_dir)?;

    extract_campaign_metadata(campaign, campaign_dir.clone())?;

    let mut path_buf = campaign_dir.clone();
    path_buf.push(&filename.as_ref());
    path_buf.set_extension("txt");

    let mut header_file = File::create(&path_buf)?;
    header_file.write_all(campaign.header.to_string().as_ref())?;

    if let Some(ref data) = campaign.image_data {
        path_buf.set_file_name("campaign_image");
        path_buf.set_extension(IMAGE_FILE_EXT);

        let mut image_file = File::create(&path_buf)?;
        image_file.write_all(data)?;
    }

    for scenario in &campaign.scenarios {
        path_buf.set_file_name(&campaign
            .header
            .decision_blocks[scenario.idx]
            .scenario_filename
        );

        path_buf.set_extension(SCENARIO_FILE_EXT);

        let mut scenario_file = File::create(&path_buf)?;
        scenario_file.write_all(&scenario.data)?;
    }

    Ok(())
}

/// Write the campaign metadata file.
fn extract_campaign_metadata(
    campaign: &Campaign,
    mut dir_path: PathBuf
) -> Result<()>
{
    dir_path.push("campaign_metadata");
    dir_path.set_extension("txt");

    let mut file = File::create(dir_path)?;

    file.write_all(format!(
        concat!(
            "Title: {}\n",
            "Description: {}\n",
            "Required Modules: {}\n",
            "Mod tags: {}"
        ),
        campaign.header.title.as_ref().map_or("", |string| string.as_str()),
        campaign.header.desc.as_ref().map_or("", |string| string.as_str()),
        campaign.modules.as_ref().map_or(String::from(""), |vec| vec.join(", ")),
        campaign.mod_tags.as_ref().map_or(String::from(""), |vec| vec.join(", "))
    ).trim().as_bytes())?;

    Ok(())
}