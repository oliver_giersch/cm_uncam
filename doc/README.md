__Note__ It appears the file layouts not only differ between engine versions, but are also inherently
different for CMSF1 as opposed to any later games.

The campaign decision block appears to have changed at some point for CMSF1 nonetheless, with 2 extra parameters
having been added.

One potential solution seems to be split of CMSF1 parsing into a special _legacy_ module, which would implement
two specializations of the CampaignParser trait, one for the earlier scenario layout and one for later one.

All other games could then be parsed with only version dependent differences